<div id="content">

    <?php
    if ($msg):
        ?>
        <div id="msg"><b><?php echo $lang->get('msg') ?>: </b><?php echo $msg ?></div>
    <?php
    endif;
    ?>

    <?php
    if ($frm)
        echo $frm;
    ?>

    <?php
    if (verify_user($db, USER_PERMISSION_VM_CREATE)):
        ?>
        <div style="padding: 10px; font-size: 14px; font-weight: bold; width: 100%; border: 1px solid grey;margin-bottom: 10px;">
            <a href="?page=new-vm"><?php echo $lang->get('create-new-vm') ?></a>
        </div>
    <?php
    endif;
    ?>

        <div id="main-menu">
            <a class="nice radius white button"href="http://localhost:8080/job/SLES%2011/build?delay=3sec">Build SLES 11 PC</a>
            <a class="nice radius white button"href="http://localhost:8080/job/SLES%2012/build?delay=3sec">Build SLES 12 PC</a>
            <a class="nice radius white button"href="http://localhost:8080/job/UpgradeDVD/build?delay=3sec">Build 11-Up</a>
            <a class="nice radius white button"href="http://localhost:8080/job/Upgrade12DVD/build?delay=3sec">Build 12-Up</a>
            <a class="nice radius blue button" href="http://localhost:8080/job/ThinClient/build?delay=0sec">Thin Client [BR]</a>
    </div>

</div>
