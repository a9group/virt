Name:		virt
Version:	0.0.4
Release:	1%{?dist}%{?extra_release}
Summary:	PHP-based virtual machine control tool
Group:		Applications/Internet
License:	GPLv3
URL:		http://bitbucket.org/a9group/virt
Source0:        http:///bitbucket.org/a9group/virt/virt-%{version}.tar.gz
BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root
BuildRequires:	gcc
Requires:	php-libvirt >= 0.4.4
Requires:	httpd
Requires:	php
Requires:	php-gd
Requires:	php-mysql

%description
virt is a virtual machine control tool written in PHP language
to allow virtual machine management using libvirt-php extension.

%prep
%setup -q -n virt-%{version}

%{__cat} <<EOF >virt.conf
#
#  %{summary}
#

<Directory "%{_datadir}/virt">
  Order Deny,Allow
  Deny from all
  Allow from 127.0.0.1
  Allow from ::1
</Directory>

<Directory "%{_datadir}/virt/data">
  Deny from all
</Directory>

<Directory "%{_datadir}/virt/logs">
  Deny from all
</Directory>

Alias /virt %{_datadir}/%{name}
EOF

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/%{_datadir}/%{name}
mkdir -p %{buildroot}/%{_datadir}/%{name}/logs
mkdir -p %{buildroot}/%{_sysconfdir}/httpd/conf.d/
mkdir -p %{buildroot}/%{_sysconfdir}/%{name}

mkdir -p %{buildroot}/%{_bindir}
gcc -o %{buildroot}/%{_bindir}/apache-key-copy tools/apache-key-copy.c

install -d -m0755 %{buildroot}%{_datadir}/%{name}/
cp -af *.php %{buildroot}%{_datadir}/%{name}/
cp -af *.css %{buildroot}%{_datadir}/%{name}/
cp -af classes/ data/ graphics/ lang/ pages/ %{buildroot}%{_datadir}/%{name}/
cp -af logs/README %{buildroot}%{_datadir}/%{name}/logs
cp -af config/connection.php %{buildroot}/%{_sysconfdir}/%{name}/connection.php
cp -af config/mysql-connection.php %{buildroot}/%{_sysconfdir}/%{name}/mysql-connection.php
install -Dp -m0644 auth/50-org.libvirt-remote-access.pkla %{buildroot}/etc/polkit-1/localauthority/50-local.d/50-org.libvirt-remote-access.pkla
install -Dp -m0644 virt.conf %{buildroot}%{_sysconfdir}/httpd/conf.d/virt.conf
chmod 777 %{buildroot}%{_datadir}/%{name}/logs

%clean
rm -rf %{buildroot}

%files
%doc AUTHORS COPYING README INSTALL
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/%{name}/connection.php
%config(noreplace) %{_sysconfdir}/%{name}/mysql-connection.php
%config(noreplace) %{_sysconfdir}/httpd/conf.d/virt.conf
%config(noreplace) /etc/polkit-1/localauthority/50-local.d/50-org.libvirt-remote-access.pkla
%{_bindir}/apache-key-copy
%{_datadir}/%{name}/

%changelog
