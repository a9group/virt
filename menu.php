  <!-- MENU -->

  <div id="menu">
<?php
	if (($lv->domain_is_running($res, $name) && ($lv->supports('screenshot'))))
		echo '<br /> <a class="nice radius large gray button" href="?name='.$name.'&amp;page=screenshot">'.$lang->get('menu-screenshot').'</a>';
?>

   <a class="nice radius large gray button" href="?name=<?php echo $name ?>"><?php echo $lang->get('menu-overview') ?></a>
   <a class="nice radius large gray button" href="?name=<?php echo $name ?>&amp;page=processor"><?php echo $lang->get('menu-processor') ?></a>
   <a class="nice radius large gray button" href="?name=<?php echo $name ?>&amp;page=memory"><?php echo $lang->get('menu-memory') ?></a>
   <a class="nice radius large gray button" href="?name=<?php echo $name ?>&amp;page=boot-options"><?php echo $lang->get('menu-boot') ?></a>
   <a class="nice radius large gray button" href="?name=<?php echo $name ?>&amp;page=disk-devices"><?php echo $lang->get('menu-disk') ?></a>
   <a class="nice radius large gray button" href="?name=<?php echo $name ?>&amp;page=network-devices"><?php echo $lang->get('menu-network') ?></a>
   <a class="nice radius large gray button" href="?name=<?php echo $name ?>&amp;page=multimedia-devices"><?php echo $lang->get('menu-multimedia') ?></a>
   <a class="nice radius large gray button" href="?name=<?php echo $name ?>&amp;page=host-devices"><?php echo $lang->get('menu-hostdev') ?></a>
  </div>
